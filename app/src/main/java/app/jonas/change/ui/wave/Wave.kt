package app.jonas.change.ui.wave

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import app.jonas.change.ui.theme.ChangeTheme

@Composable
fun WaveRoute() {
    WaveScreen()
}

const val STEPS = 8

val widthRange = 8f..120f
val heightRange = 0f..16f
val thicknessRange = 1f..10f
val speedRange = 0f..1f

fun ClosedFloatingPointRange<Float>.valueAt(step: Int) = start + ((endInclusive - start) / (STEPS + 1)) * step

@Composable
private fun WaveScreen() {
    var width by remember { mutableStateOf(value = widthRange.valueAt(step = 8)) }
    var height by remember { mutableStateOf(value = heightRange.valueAt(step = 0)) }
    var thickness by remember { mutableStateOf(value = thicknessRange.valueAt(step = 5)) }
    var speed by remember { mutableStateOf(value = speedRange.valueAt(step = 3)) }
    Column(
        modifier = Modifier
            .padding(32.dp)
            .waveBorder(
                color = MaterialTheme.colorScheme.primary,
                wave = Wave(
                    width = width.dp,
                    height = height.dp,
                    thickness = thickness.dp,
                    speed = speed,
                ),
            )
            .padding(8.dp),
    ) {
        Slider(
            value = width,
            onValueChange = { width = it },
            valueRange = widthRange,
            steps = STEPS,
            onValueChangeFinished = {},
        )
        Slider(
            value = height,
            onValueChange = { height = it },
            valueRange = heightRange,
            steps = STEPS,
            onValueChangeFinished = {},
        )
        Slider(
            value = thickness,
            onValueChange = { thickness = it },
            valueRange = thicknessRange,
            steps = STEPS,
            onValueChangeFinished = {},
        )
        Slider(
            value = speed,
            onValueChange = { speed = it },
            valueRange = speedRange,
            steps = STEPS,
            onValueChangeFinished = {},
        )
    }
}

@Preview(showBackground = true)
@Composable
fun WavePreview() {
    ChangeTheme {
        WaveScreen()
    }
}
