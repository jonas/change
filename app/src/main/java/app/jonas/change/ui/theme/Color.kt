@file:Suppress("MagicNumber")

package app.jonas.change.ui.theme

import androidx.compose.ui.graphics.Color

val PrimaryDark = Color(0xFFd7bcbc)
val PrimaryContainerDark = Color(0xFF978484)
val SecondaryDark = Color(0xFFd2d7bc)
val TertiaryDark = Color(0xFFbcc7d7)

val PrimaryLight = Color(0xFFbc8f8f)
val PrimaryContainerLight = Color(0xFFd0b1b1)
val SecondaryLight = Color(0xFFb3bc8f)
val TertiaryLight = Color(0xFF8fa1bc)
