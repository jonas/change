@file:Suppress("MatchingDeclarationName")

package app.jonas.change.ui.wave

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.CompositingStrategy
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import app.jonas.change.ui.theme.ChangeTheme

data class Wave(val width: Dp, val height: Dp, val thickness: Dp, val speed: Float) {
    val isAnimated = height != 0.dp && speed != 0f
}

private object NoDx : State<Float> {
    override val value = 0f
}
const val MAX_DURATION = 500

fun Modifier.waveBorder(color: Color, wave: Wave): Modifier = composed {
    /**
     * This hack is needed because InfiniteTransition.animateValue(...) ignores animationSpec updates
     * and only updates when initial or target value changes.
     * Note that the result of the animation is later divided by the toggling target value to normalize it.
     */
    var targetValue by remember { mutableStateOf(1f) }
    LaunchedEffect(wave.speed) {
        targetValue = if (targetValue == 1f) 2f else 1f
    }
    val dx by if (!wave.isAnimated) {
        NoDx
    } else {
        val deltaXAnim = rememberInfiniteTransition(label = "delta x")
        deltaXAnim.animateFloat(
            initialValue = 0f,
            targetValue = targetValue,
            animationSpec = infiniteRepeatable(
                animation = tween((MAX_DURATION / wave.speed).toInt(), easing = LinearEasing),
            ),
            label = "delta x",
        )
    }
    val path = Path()

    this then graphicsLayer {
        compositingStrategy = CompositingStrategy.Offscreen
    } then drawBehind {
        val waveHeightPx = wave.height.toPx()
        val waveWidthPx = wave.width.roundToPx()
        val waveThicknessPx = wave.thickness.toPx()

        drawPath(path = path, color = color, 1f, Stroke(waveThicknessPx))
        path.reset()
        val halfWaveWidth = waveWidthPx / 2

        fun drawHorizontal() {
            (-waveWidthPx..(size.width.toInt() + waveWidthPx) step waveWidthPx).forEach { _ ->
                path.relativeQuadraticBezierTo(
                    halfWaveWidth.toFloat() / 2,
                    -waveHeightPx,
                    halfWaveWidth.toFloat(),
                    0f,
                )
                path.relativeQuadraticBezierTo(
                    halfWaveWidth.toFloat() / 2,
                    waveHeightPx,
                    halfWaveWidth.toFloat(),
                    0f,
                )
            }
        }

        fun drawVertical() {
            (-waveWidthPx..(size.height.toInt() + waveWidthPx) step waveWidthPx).forEach { _ ->
                path.relativeQuadraticBezierTo(
                    -waveHeightPx,
                    halfWaveWidth.toFloat() / 2,
                    0f,
                    halfWaveWidth.toFloat(),
                )
                path.relativeQuadraticBezierTo(
                    waveHeightPx,
                    halfWaveWidth.toFloat() / 2,
                    0f,
                    halfWaveWidth.toFloat(),
                )
            }
        }

        val waveOffsetWidth = waveWidthPx * (dx / targetValue)
        val waveOffsetHeight = waveHeightPx + if (waveHeightPx < waveThicknessPx) waveThicknessPx / 2 else 0f
        path.moveTo(-waveWidthPx + waveOffsetWidth, 0 + waveOffsetHeight)
        drawHorizontal()
        path.moveTo(0 - waveOffsetWidth, size.height - waveOffsetHeight)
        drawHorizontal()
        path.moveTo(0f + waveOffsetHeight, 0 - waveOffsetWidth)
        drawVertical()
        path.moveTo(size.width - waveOffsetHeight, -waveWidthPx + waveOffsetWidth)
        drawVertical()

        @Suppress("MagicNumber")
        val circleOffsetFromCorner = waveHeightPx * 0.75f
        drawCircle(color, radius = waveHeightPx * 2f, center = Offset(circleOffsetFromCorner, circleOffsetFromCorner))
        drawCircle(
            color,
            radius = waveHeightPx * 2f,
            center = Offset(size.width - circleOffsetFromCorner, circleOffsetFromCorner),
        )
        drawCircle(
            color,
            radius = waveHeightPx * 2f,
            center = Offset(size.width - circleOffsetFromCorner, size.height - circleOffsetFromCorner),
        )
        drawCircle(
            color,
            radius = waveHeightPx * 2f,
            center = Offset(circleOffsetFromCorner, size.height - circleOffsetFromCorner),
        )
    } then padding(max(wave.height * 2, wave.thickness))
}

@Preview(showBackground = true)
@Composable
fun WaveBorderPreview() {
    ChangeTheme {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1f)
                .padding(16.dp)
                .waveBorder(
                    color = MaterialTheme.colorScheme.primary,
                    wave = Wave(
                        width = 120.dp,
                        height = 16.dp,
                        thickness = 8.dp,
                        speed = 0.3f,
                    ),
                ),
        )
    }
}
