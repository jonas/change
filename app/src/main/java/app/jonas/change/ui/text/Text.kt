package app.jonas.change.ui.text

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import app.jonas.change.ui.theme.ChangeTheme
import app.jonas.change.ui.wave.Wave
import app.jonas.change.ui.wave.waveBorder

@Composable
fun TextRoute() {
    TextScreen()
}

@Composable
private fun TextScreen() {
    Column(modifier = Modifier.padding(32.dp), verticalArrangement = Arrangement.Center) {
        Box(
            modifier = Modifier
                .waveBorder(
                    color = MaterialTheme.colorScheme.primary,
                    wave = Wave(
                        width = 120.dp,
                        height = 8.dp,
                        thickness = 4.dp,
                        speed = 0.3f,
                    ),
                )
                .padding(8.dp),
        ) {
            TextField(value = "", onValueChange = {}, modifier = Modifier.fillMaxWidth())
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TextPreview() {
    ChangeTheme {
        TextScreen()
    }
}
