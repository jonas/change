package app.jonas.change.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import app.jonas.change.ui.text.TextRoute
import app.jonas.change.ui.wave.WaveRoute

@Composable
fun ChangeNavHost() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = WaveDestination.route) {
        composable(WaveDestination.route) {
            WaveRoute()
        }

        composable(TextDestination.route) {
            TextRoute()
        }
    }
}
