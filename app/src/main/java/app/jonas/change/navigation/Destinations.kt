package app.jonas.change.navigation

interface Destination {
    val route: String
}

object WaveDestination : Destination {
    override val route = "wave"
}

object TextDestination : Destination {
    override val route = "text"
}
