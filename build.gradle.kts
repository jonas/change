import org.jetbrains.gradle.ext.settings
import org.jetbrains.gradle.ext.taskTriggers
import org.jmailen.gradle.kotlinter.tasks.FormatTask
import org.jmailen.gradle.kotlinter.tasks.InstallHookTask
import org.jmailen.gradle.kotlinter.tasks.LintTask

// Top-level build file where you can add configuration options common to all sub-projects/modules.
@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.idea.ext)
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.kotlinAndroid) apply false
    alias(libs.plugins.kotlinter) apply false
}

val ktsFiles = arrayOf(
    file("settings.gradle.kts"),
    file("build.gradle.kts"),
    file("app/build.gradle.kts"),
)

val installKotlinterPreCommitHook = "installKotlinterPreCommitHook"

abstract class InstallCustomPrePushHook : InstallHookTask("pre-push") {
    override val hookContent =
        """
            if ! ${'$'}GRADLEW lintBuildscripts lintKotlin detektMain ; then
                echo 1>&2 "\nlintBuildscripts, lintKotlin or detektMain found problems, running formatBuildscripts && formatKotlin; commit the result and re-push"
                ${'$'}GRADLEW formatBuildscripts formatKotlin
                exit 1
            fi
        """.trimIndent()
}

tasks {

    register<LintTask>("lintBuildscripts") {
        group = "formatting"
        source(*ktsFiles)
    }

    register<FormatTask>("formatBuildscripts") {
        group = "formatting"
        source(*ktsFiles)
    }

    register(installKotlinterPreCommitHook, InstallCustomPrePushHook::class.java) {
        group = "build setup"
        description = "Installs Kotlinter Git pre-push hook"
    }
}

// https://stackoverflow.com/a/61285609
idea.project.settings {
    taskTriggers {
        afterSync(tasks.getByName(installKotlinterPreCommitHook))
    }
}
